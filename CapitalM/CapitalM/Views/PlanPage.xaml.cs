﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CapitalM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlanPage : ContentPage
    {
        public PlanPage()
        {
            InitializeComponent();
        }
    }
}