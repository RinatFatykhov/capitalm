﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CapitalM.Models;
using CapitalM.ViewModels;

namespace CapitalM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemDetailPage : ContentPage
    {
        ItemDetailViewModel viewModel;

        public ItemDetailPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;
        }

        public ItemDetailPage()
        {
            InitializeComponent();
            var record = new Record
            {
                Title = "",
                Description = ""
            };
            viewModel = new ItemDetailViewModel(record);
            BindingContext = viewModel;
        }
    }
}