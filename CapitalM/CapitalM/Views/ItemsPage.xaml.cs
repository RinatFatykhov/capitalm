﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CapitalM.Models;
using CapitalM.Views;
using CapitalM.ViewModels;

namespace CapitalM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel viewModel;

        public ItemsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ItemsViewModel();
        }
        

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {

            //var selectedItem = args.SelectedItem as Record;
            //if (selectedItem == null)
            //    return;
            //await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(selectedItem)));
            //ItemsListView.SelectedItem = null;

            if (!(args.SelectedItem is Record selectedItem))
            {
                return;
            }

            await Navigation.PushAsync(new NewItemPage(selectedItem));
            ItemsListView.SelectedItem = null;

            //Item selectedItem = (Item)args.SelectedItem;
            //NewItemPage newItemPage = new NewItemPage();
            //newItemPage.BindingContext = selectedItem;
            //newItemPage.Item = selectedItem;
            //await Navigation.PushAsync(newItemPage);
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewItemPage());
            //await Navigation.PushModalAsync(itemPage);
            //await Navigation.PushModalAsync(new NavigationPage(new NewItemPage()));
        }

        protected override void OnAppearing()
        {
            ItemsListView.ItemsSource = App.Database.GetRecords();
            base.OnAppearing();
        }
    }
}