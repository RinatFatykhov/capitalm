﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CapitalM.Models;

namespace CapitalM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewItemPage : ContentPage
    {
        public Record Record { get; set; }

        public NewItemPage()
        {
            InitializeComponent();
            Record = new Record
            {
                Title = "",
                Description = "",
                Category = "",
                Amount = 0,
                Time = DateTime.Now.ToLocalTime(),
                PathSource = "",
                PaymentMethod=""
            };
            BindingContext = Record;
        }
        public NewItemPage(Record Record)
        {
            InitializeComponent();
            BindingContext = this.Record = Record;
            Title.Text = this.Record.Title;
            for (int i = 0; i < 5; i++)
                if (Category.Items[i] == this.Record.Category)
                {
                    Category.SelectedIndex = i;
                    break;
                }
                    

            Amount.Text = this.Record.Amount.ToString();
            Description.Text = this.Record.Description;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            var Record = (Record)BindingContext;
            if (!String.IsNullOrEmpty(Record?.Title))
            {
                if (Category.SelectedIndex != -1)
                {
                    if (String.IsNullOrWhiteSpace(Amount.Text) || Amount.Text == null)
                    {
                        DisplayAlert("", "Введите сумму!", "Oк");
                        return;
                    }
                    Record.Category = Category.Items[Category.SelectedIndex];
                    App.Database.SaveRecord(Record);
                    await Navigation.PopAsync();
                }
                else DisplayAlert("", "Выберите категорию!", "Oк");
            }
            else
            {
                DisplayAlert("", "Пустой заголовок!","Oк");
            }
        }
        async void DeleteRecord(object sender, EventArgs e)
        {
            var Record = (Record)BindingContext;
            App.Database.DeleteRecord(Record.Id);
            await Navigation.PopAsync();
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }


    }
}