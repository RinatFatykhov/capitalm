﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CapitalM.Models
{
    public enum MenuItemType
    {
        Current_balance,
        Plan,
        Sheet 
    }

    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }
        public string Title { get; set; }
    }
}
