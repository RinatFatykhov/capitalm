﻿using System;
using SQLite;
using System.ComponentModel;

namespace CapitalM.Models
{
    [Table("Records")]
    public class Record 
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        public string Title { get; set; }
        public string Category { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set;}
        public DateTime Time { get; set; }

        [Ignore]
        public string PathSource { get; set; }
        [Ignore]
        public string PaymentMethod { get; set; }
    }
}