﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CapitalM.Services;
using CapitalM.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace CapitalM
{
    public partial class App : Application
    {
        //TODO: Replace with *.azurewebsites.net url after deploying backend to Azure
        public static string AzureBackendUrl = "http://localhost:5000";
        public static bool UseMockDataStore = true;

        public const string DATABASE_NAME = "Records.db";
        public static RecordRepository database;
        public static RecordRepository Database
        {
            get
            {
                if (database == null)
                {
                    database = new RecordRepository(DATABASE_NAME);
                }
                return database;
            }
        }
        public App()
        {
            InitializeComponent();

            
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
