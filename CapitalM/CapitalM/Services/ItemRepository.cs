﻿using System.Collections.Generic;
using System.Linq;
using SQLite;
using Xamarin.Forms;
using CapitalM.Models;

namespace CapitalM.Services
{
    public class RecordRepository
    {
        SQLiteConnection database;
        public RecordRepository(string filename)
        {
            string databasePath = DependencyService.Get<ISQLite>().GetDatabasePath(filename);
            database = new SQLiteConnection(databasePath);
            database.CreateTable<Record>();
        }
        public IEnumerable<Record> GetRecords()
        {
            return (from i in database.Table<Record>() select i).ToList();
        }
        public Record GetRecord(int id)
        {
            return database.Get<Record>(id);
        }
        public int DeleteRecord(int id)
        {
            return database.Delete<Record>(id);
        }
        public int SaveRecord(Record Record)
        {
            if (Record.Id != 0)
            {
                database.Update(Record);
                return Record.Id;
            }
            else
            {
                return database.Insert(Record);
            }
        }
    }
}