﻿using System;

using CapitalM.Models;

namespace CapitalM.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Record Record { get; set; }
        public ItemDetailViewModel(Record record = null)
        {
            Title ="Время "+record?.Time;
            Record = record;
        }
    }
}
