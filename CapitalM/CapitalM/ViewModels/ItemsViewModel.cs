﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using CapitalM.Models;
using CapitalM.Views;

namespace CapitalM.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableCollection<Record> Records { get; set; }
        public Command LoadItemsCommand { get; set; }

        public ItemsViewModel()
        {
            Title = "Текущие расходы";
            Records = new ObservableCollection<Record>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NewItemPage, Record>(this, "Добавление заметки", async (obj, record) =>
            {
                var newItem = record as Record;
                Records.Add(newItem);
              
            });
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Records.Clear();
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}