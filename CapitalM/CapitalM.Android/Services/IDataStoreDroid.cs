﻿using System;
using CapitalM.Droid;
using System.IO;
using Xamarin.Forms;
using CapitalM.Services;

[assembly: Dependency(typeof(SQLite_Android))]
namespace CapitalM.Droid
{
    public class SQLite_Android : ISQLite
    {
        public SQLite_Android() { }
        public string GetDatabasePath(string sqliteFilename)
        {
            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var path = Path.Combine(documentsPath, sqliteFilename);
            return path;
        }
    }
}